package com.threadjava.exceptions;

public class IllegalPostAccessException extends RuntimeException {

    public IllegalPostAccessException() {
        super();
    }

    public IllegalPostAccessException(String message) {
        super(message);
    }

    public IllegalPostAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalPostAccessException(Throwable cause) {
        super(cause);
    }

}
