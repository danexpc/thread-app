package com.threadjava.post.dto;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class PostCommentDto {
    private UUID id;
    private String body;
    private PostUserDto user;
    private UUID postId;
    private long likeCount;
    private long dislikeCount;
    private Date createdAt;
    private Date updatedAt;
}
