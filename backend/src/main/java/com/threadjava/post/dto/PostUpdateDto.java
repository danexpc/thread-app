package com.threadjava.post.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class PostUpdateDto {
    private UUID postId;
    private UUID imageId;
    private String body;
}
