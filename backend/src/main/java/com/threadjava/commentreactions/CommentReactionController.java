package com.threadjava.commentreactions;

import com.threadjava.commentreactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentreactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/commentreactions")
public class CommentReactionController {

    @Autowired
    private CommentReactionService commentService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponseCommentReactionDto> setReaction(@RequestBody ReceivedCommentReactionDto commentReaction){
        commentReaction.setUserId(getUserId());
        var reaction = commentService.setReaction(commentReaction);

        if (reaction.isPresent() && reaction.get().getUserId() != getUserId()) {
            template.convertAndSend("/topic/like", "Your comment was liked!");
        }

        return reaction;
    }

}
