package com.threadjava.commentreactions;

import com.threadjava.commentreactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentreactions.dto.ResponseCommentReactionDto;
import com.threadjava.commentreactions.model.CommentReaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentReactionMapper {

    CommentReactionMapper MAPPER = Mappers.getMapper( CommentReactionMapper.class );

    @Mapping(source = "comment.id", target = "commentId")
    @Mapping(source = "user.id", target = "userId")
    ResponseCommentReactionDto reactionToCommentReactionDto(CommentReaction commentReaction);

    @Mapping(source = "userId", target = "user.id")
    @Mapping(source = "commentId", target = "comment.id")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    CommentReaction dtoToCommentReaction(ReceivedCommentReactionDto commentReactionDto);
}
