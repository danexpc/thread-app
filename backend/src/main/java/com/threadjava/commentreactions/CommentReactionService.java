package com.threadjava.commentreactions;

import com.threadjava.commentreactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentreactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentReactionService {

    @Autowired
    private CommentReactionRepository commentReactionRepository;

    public Optional<ResponseCommentReactionDto> setReaction(ReceivedCommentReactionDto commentReactionDto) {

        var reaction = commentReactionRepository.getCommentReaction(
                commentReactionDto.getUserId(),
                commentReactionDto.getCommentId()
        );

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike().equals(commentReactionDto.getIsLike())) {
                commentReactionRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(commentReactionDto.getIsLike());
                var result = commentReactionRepository.save(react);
                return Optional.of(CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result));
            }
        } else {
            var commentReaction = CommentReactionMapper.MAPPER.dtoToCommentReaction(commentReactionDto);
            var result = commentReactionRepository.save(commentReaction);
            return Optional.of(CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result));
        }
    }
}
