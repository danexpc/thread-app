package com.threadjava.commentreactions.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class CommentReactionDto {
    private UUID id;
    private Boolean isLike;
}
