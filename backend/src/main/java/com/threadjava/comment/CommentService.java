package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentInfoDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import com.threadjava.comment.model.Comment;
import com.threadjava.exceptions.IllegalPostAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    public CommentInfoDto getCommentById(UUID id) {
        return commentRepository.findCommentByCommentId(id)
                .map(CommentMapper.MAPPER::commentQueryDtoToCommentInfoDto)
                .orElseThrow();
    }

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var postCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(postCreated);
    }

    public CommentDetailsDto update(CommentUpdateDto commentDto) {
        var commentId = commentDto.getId();

        var comment = getCommentIfAccessAllowed(commentId);
        comment.setBody(commentDto.getBody());

        var updatedComment = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(updatedComment);
    }

    public CommentDetailsDto delete(UUID id) {
        var comment = getCommentIfAccessAllowed(id);
        comment.setDeleted(true);
        var deletedComment = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(deletedComment);
    }

    private Comment getCommentIfAccessAllowed(UUID commentId) {
        var comment = commentRepository.findById(commentId).orElseThrow();

        var commentOwner = comment.getUser();
        if (!commentOwner.getId().equals(getUserId())) {
            throw new IllegalPostAccessException("Current user cannot modify post");
        }

        return comment;
    }

}
