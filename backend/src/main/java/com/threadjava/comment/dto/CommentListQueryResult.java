package com.threadjava.comment.dto;

import com.threadjava.users.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentListQueryResult {
    public UUID id;
    public String body;
    public User user;
    public UUID postId;
    public long likeCount;
    public long dislikeCount;
    public boolean isDeleted;
    public Date createdAt;
    public Date updatedAt;
}
