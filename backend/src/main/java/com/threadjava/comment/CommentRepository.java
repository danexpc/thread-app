package com.threadjava.comment;

import com.threadjava.comment.dto.CommentListQueryResult;
import com.threadjava.comment.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {

    @Query("SELECT new com.threadjava.comment.dto.CommentListQueryResult(c.id, c.body, c.user, c.post.id, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.isDeleted, c.createdAt, c.updatedAt) " +
            "FROM Comment c " +
            "WHERE (:postId = c.post.id) AND (c.isDeleted = FALSE)" +
            "ORDER BY c.createdAt")
    List<CommentListQueryResult> findAllCommentsByPostId(@Param("postId") UUID postId);

    @Query("SELECT new com.threadjava.comment.dto.CommentListQueryResult(c.id, c.body, c.user, c.post.id, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.isDeleted, c.createdAt, c.updatedAt) " +
            "FROM Comment c " +
            "WHERE (:id = c.id) AND (c.isDeleted = FALSE)")
    Optional<CommentListQueryResult> findCommentByCommentId(@Param("id") UUID id);

}