import * as React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI } from 'semantic-ui-react';
import { Label } from '../../../common/common';
import styles from './styles.module.scss';

const CommentAction = ({ children, onClick }) => (
  <CommentUI.Action onClick={onClick}>
    <Label
      basic
      size="small"
      color="grey"
      className={styles.toolbarBtn}
    >
      {children}
    </Label>
  </CommentUI.Action>
);

CommentAction.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  onClick: PropTypes.func.isRequired
};

export default CommentAction;
