import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Spinner, Post, Modal, Comment as CommentUI, Confirm } from 'src/components/common/common';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';
import UpdateComment from '../update-comment/update-comment';

const ExpandedPost = ({ sharePost, onUpdatedPostToggle, onPostDelete }) => {
  const dispatch = useDispatch();
  const { post, userId } = useSelector(state => ({
    post: state.posts.expandedPost,
    userId: state.profile.user.id
  }));

  const [updatedCommentId, setUpdatedCommentId] = React.useState('');
  const [deletedCommentId, setDeletedCommentId] = React.useState('');

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.ratePost(id))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.ratePost(id, false))
  ), [dispatch]);

  const handleCommentAdd = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleCommentUpdate = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.updateComment(commentPayload))
  ), [dispatch]);

  const handleCommentDelete = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.deleteComment(commentPayload))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handleCommentLike = React.useCallback(id => (
    dispatch(threadActionCreator.rateComment(id))
  ), [dispatch]);

  const handleCommentDislike = React.useCallback(id => (
    dispatch(threadActionCreator.rateComment(id, false))
  ), [dispatch]);

  const handleExpandedPostClose = () => handleExpandedPostToggle();

  const sortedComments = getSortedComments(post.comments ?? []);

  const handlePostDelete = id => {
    handleExpandedPostClose();
    onPostDelete(id);
  };

  const onCommentUpdate = payload => {
    handleCommentUpdate(payload);
    setUpdatedCommentId('');
  };

  const clearDeletedComment = () => setDeletedCommentId('');

  const deleteComment = () => {
    handleCommentDelete(deletedCommentId);
    clearDeletedComment();
  };

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <Modal.Content>
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            currentUserId={userId}
            onUpdatedPostToggle={onUpdatedPostToggle}
            onPostDelete={handlePostDelete}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => {
              if (updatedCommentId !== comment.id) {
                return (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    onUpdate={() => setUpdatedCommentId(comment.id)}
                    onDelete={() => setDeletedCommentId(comment.id)}
                    currentUserId={userId}
                    onCommentLike={() => handleCommentLike(comment.id)}
                    onCommentDislike={() => handleCommentDislike(comment.id)}
                  />
                );
              }
              return (
                <UpdateComment
                  key={comment.id}
                  onCommentUpdate={onCommentUpdate}
                  commentId={comment.id}
                  postId={post.id}
                  commentBody={comment.body}
                />
              );
            })}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </CommentUI.Group>
          <Confirm
            open={!!deletedCommentId}
            onConfirm={deleteComment}
            onCancel={clearDeletedComment}
            content="Are you sure you want to delete this comment?"
          />
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  sharePost: PropTypes.func.isRequired,
  onUpdatedPostToggle: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func.isRequired
};

export default ExpandedPost;
