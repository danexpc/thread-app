import * as React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Spinner, Modal, Form, Image, Button } from 'src/components/common/common';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const UpdatePost = ({ uploadImage, onPostUpdate, onUpdatedPostToggle }) => {
  const { post } = useSelector(state => ({
    post: state.posts.updatedPost
  }));

  const [body, setBody] = React.useState(post.body);
  const [image, setImage] = React.useState(post.image ? { ...post.image } : null);
  const [isUploading, setIsUploading] = React.useState(false);

  const handleUpdatedPostClose = () => onUpdatedPostToggle();

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }

    await onPostUpdate({
      postId: post.id,
      imageId: image?.id,
      body
    });
    setBody('');
    setImage(undefined);
    handleUpdatedPostClose();
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id, link }) => {
        setImage({ id, link });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={handleUpdatedPostClose}
    >
      {post ? (
        <Modal.Content>
          <Form onSubmit={handleUpdatePost}>
            <Form.TextArea
              name="body"
              value={body}
              placeholder="What is the news?"
              onChange={ev => setBody(ev.target.value)}
            />
            {image?.link && (
              <div className={styles.imageWrapper}>
                <Image className={styles.image} src={image?.link} alt="post" />
              </div>
            )}
            <div className={styles.btnWrapper}>
              <Button
                color="teal"
                isLoading={isUploading}
                isDisabled={isUploading}
                iconName={IconName.IMAGE}
              >
                <label className={styles.btnImgLabel}>
                  {image ? 'Change image' : 'Attach image'}
                  <input
                    name="image"
                    type="file"
                    onChange={handleUploadFile}
                    hidden
                  />
                </label>
              </Button>
              <Button
                color={ButtonColor.BLUE}
                type={ButtonType.SUBMIT}
              >
                Edit
              </Button>
            </div>
          </Form>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

UpdatePost.propTypes = {
  uploadImage: PropTypes.func.isRequired,
  onPostUpdate: PropTypes.func.isRequired,
  onUpdatedPostToggle: PropTypes.func.isRequired
};

export default UpdatePost;
