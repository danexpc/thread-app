import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';
import CommentAction from '../comment-action/comment-action';

import styles from './styles.module.scss';

const Comment = ({
  comment: { body, createdAt, user, likeCount, dislikeCount },
  onUpdate, onDelete,
  currentUserId,
  onCommentLike,
  onCommentDislike
}) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
    <CommentUI.Content>
      <CommentUI.Author as="a">{user.username}</CommentUI.Author>
      <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
      <CommentUI.Text>{body}</CommentUI.Text>
      <CommentUI.Actions>
        <CommentAction onClick={onCommentLike}>
          <Icon name={IconName.THUMBS_UP} />
          { likeCount }
        </CommentAction>
        <CommentAction onClick={onCommentDislike}>
          <Icon name={IconName.THUMBS_DOWN} />
          { dislikeCount }
        </CommentAction>
        {currentUserId === user.id && (
          <>
            <CommentAction onClick={onUpdate}>
              Edit
            </CommentAction>
            <CommentAction onClick={onDelete}>
              Delete
            </CommentAction>
          </>
        )}
      </CommentUI.Actions>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: commentType.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  currentUserId: PropTypes.string.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired
};

export default Comment;
