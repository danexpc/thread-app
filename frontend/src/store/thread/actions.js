import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  UPDATE_POST: 'thread/update-post',
  SET_UPDATED_POST: 'thread/set-updated-post',
  DELETE_POST: 'thread/delete-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const updatePost = createAction(ActionType.UPDATE_POST, post => ({
  payload: {
    post
  }
}));

const setUpdatePost = createAction(ActionType.SET_UPDATED_POST, post => ({
  payload: {
    post
  }
}));

const deletePost = createAction(ActionType.DELETE_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const ratePost = (postId, isLike = true) => async (dispatch, getRootState) => {
  await postService.ratePost(postId, isLike);
  const currentPost = await postService.getPost(postId);

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : currentPost));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(currentPost));
  }
};

const updatePostComments = (getRootState, comment, mapComments, dispatch) => {
  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment]
  });

  updatePostComments(getRootState, comment, mapComments, dispatch);
};

const updateComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.updateComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => {
    if (!post.comments) {
      return post;
    }
    const idx = post.comments.findIndex(comm => comm.id === id);
    return {
      ...post,
      comments: [
        ...post.comments.slice(0, idx),
        { ...post.comments[idx], ...comment },
        ...post.comments.slice(idx + 1)]
    };
  };

  updatePostComments(getRootState, comment, mapComments, dispatch);
};

const rateComment = (commentId, isLike = true) => async (dispatch, getRootState) => {
  await commentService.rateComment(commentId, isLike);
  const currentComment = await commentService.getComment(commentId);

  const mapComments = post => {
    if (!post.comments) {
      return post;
    }
    const idx = post.comments.findIndex(comment => comment.id === currentComment.id);
    return {
      ...post,
      comments: [
        ...post.comments.slice(0, idx),
        currentComment,
        ...post.comments.slice(idx + 1)]
    };
  };

  updatePostComments(getRootState, currentComment, mapComments, dispatch);
};

const deleteComment = request => async (dispatch, getRootState) => {
  const comment = await commentService.deleteComment(request);

  const mapComments = post => {
    if (!post.comments || !comment.id) {
      return post;
    }
    const idx = post.comments.findIndex(comm => comm.id === comment.id);
    return {
      ...post,
      commentCount: Number(post.commentCount) - 1,
      comments: [...post.comments.slice(0, idx), ...post.comments.slice(idx + 1)]
    };
  };

  updatePostComments(getRootState, comment, mapComments, dispatch);
};

const editPost = post => async dispatch => {
  const { id } = await postService.updatePost(post);
  const updatedPost = await postService.getPost(id);
  dispatch(updatePost(updatedPost));
};

const toggleUpdatedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setUpdatePost(post));
};

const removePost = postId => async dispatch => {
  const post = await postService.deletePost(postId);
  dispatch(deletePost(post));
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  updatePost,
  setUpdatePost,
  deletePost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  ratePost,
  addComment,
  updateComment,
  rateComment,
  deleteComment,
  editPost,
  toggleUpdatedPost,
  removePost
};
