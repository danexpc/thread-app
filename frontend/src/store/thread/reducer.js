import { createReducer } from '@reduxjs/toolkit';
import { setPosts, addMorePosts, addPost, setExpandedPost, updatePost, setUpdatePost, deletePost } from './actions';

const initialState = {
  posts: [],
  expandedPost: null,
  hasMorePosts: true,
  updatedPost: null
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setPosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addMorePosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addPost, (state, action) => {
    const { post } = action.payload;

    state.posts = [post, ...state.posts];
  });
  builder.addCase(setExpandedPost, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(updatePost, (state, action) => {
    const { post } = action.payload;

    state.posts = state.posts.map(item => (item.id === post.id ? post : item));
  });
  builder.addCase(setUpdatePost, (state, action) => {
    const { post } = action.payload;

    state.updatedPost = post;
  });
  builder.addCase(deletePost, (state, action) => {
    const { post } = action.payload;
    const idx = state.posts.findIndex(item => item.id === post.id);

    state.posts = [
      ...state.posts.slice(0, idx),
      ...state.posts.slice(idx + 1)
    ];
  });
});

export { reducer };
